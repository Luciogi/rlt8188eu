# What is this?
This Repository contain driver source tree of Realtek [8188EUS](https://github.com/aircrack-ng/rtl8188eus)
There is issue with upstream driver, unable to connect with APs with dynamic ip config.
# Requirement
## Build
1. kernel + headers for 5.15.x
2. Arch Linux: `base-devel`

# How to build?
```sh
git clone https://github.com/aircrack-ng/rtl8188eus
cd rtl8188eus
git checkout aa92fe1156b02e9cbd28a6aabcfb5bc581d539d0
make -j4
```
## More
```sh
echo 'blacklist r8188eu' | sudo tee -a '/etc/modprobe.d/realtek.conf'
```

## Test driver
```sh
insmod 8188.ko
```
## Install
```sh
sudo make install
```

# Precompiled binary !!
 [Download](https://codeberg.org/Luciogi/rlt8188eu/releases/tag/1.0)

# TODO
 - [ ] Make PKGBUILD
 - [ ] Make standalone combination with kernel headers
